# mdindex

***TODO*** mdindex collides with 'metadata' tools from apple (spotlight)

`mdindex` creates or updates an **Index** section for the contents of a
markdown file.

## Index

- [Usage](#usage)
- [Description](#description)
- [Options](#options)
- [Examples](#examples)
- [Copyright](#copyright)

## Usage

`% mdindex [<file|dir>...]`

## Description

`mdindex` scans markdown files, looking for headings with the word `index`
(case insensitive) in their name and *only* a list as their content.

If there are ***any*** non-list lines between an `# index` and the following heading,
then the section will be left unchanged.

A *list line* begins with a `-` or a `*`, with optional indentation (whitespace).

Existing index lists will be *replaced* by new lists of links to all following
headings at the same level (or lower) as the first heading, directly after the
index.

Since the index only lists the sections that come directly after it, the
*heading level* of the index will be changed to match the following heading.
See the [Examples](#examples) section below.

There are two ways to indicate that an index should include more than 1 level of headings.

1. by adding a single digit as the number of levels to include:

```
## Index -3
```

2. or implicitly, the *number of indent levels* of the list in the index is
   used to derive the hierarchical depth to use:

```
## Index
-
  -
    -
```

See the [Examples](#examples) section below for more.

## Options

### `<file|dir>`

Any number of file and/or directory names

If no file or directory names are provided, `mdindex` will recursively search
the current directory for file with names matching `*.md`

### `-no-run` | `-n`

Don't actually modify any files.

Implies `-verbose`.

### `-help`

Show the help for `mdindex` and exit

### `-verbose` | `-v`

Display information about each file and index as they are processed

### `-version`

Show the current version of `mdindex` and exit

## Examples

### A simple Index

**Before**

```
# My Blog
# Index
## Introduction
## Day 1
## Day 2
## The End
```

Note that the `# Index` heading only has one `#`... since the first heading
*after* the index has two (`##`), the index is also updated to match the level
of its contents.

**After**

```
# My Blog
## Index

- [Introduction](#introduction)
- [Day 1](#day-1)
- [Day 2](#day-2)
- [The End](#the-end)

## Introduction
## Day 1
## Day 2
## The End
```

### Two Indexes

**Before**

```
# Title
## Top Index
## Level 1 - Start
### Inner Index
### Level 2 - First Step
#### Level 3 - First Detail Of First Step
#### Level 3 - Second Detail Of First Step
### Level 2 - Second Step
#### Level 3 - Details of Second Step
## Level 1 - Middle
## Level 1 - End
```

Here, we have two indexes (`Top Index` and `Inner Index`), each gets its own
set of links:

**After**

```
# Title
## Top Index

- [Level 1 - Start](#level-1---start)
- [Level 1 - Middle](#level-1---middle)
- [Level 1 - End](#level-1---end)

## Level 1 - Start
### Inner Index

- [Level 2 - First Step](#level-2---first-step)
- [Level 2 - Second Step](#level-2---second-step)

### Level 2 - First Step
#### Level 3 - First Detail Of First Step
#### Level 3 - Second Detail Of First Step
### Level 2 - Second Step
#### Level 3 - Details of Second Step
## Level 1 - Middle
## Level 1 - End
```

## Copyright

Copyright 2022 Stephen Riehm japh-codeberg@opensauce.de
