#!/usr/bin/env perl

#
# test the construction of URIs for markdown headings, when the same heading us
# re-used multiple times.
#
#   Rules:
#       - uris for repeated headings are numbered
#

use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";

use Test::More;
use PSV;

use Markdown::Heading;
use Markdown::URI::Register;

my @tests = PSV->table_from_string(
    {
    line_offset    => 4,
    list_of_hashes => 1,
    },
    q{

    | document | base      | deduplicated | comments                          |
    | -------- | --------- | ------------ | --------------------------------- |

    | examples | example   | example      |                                   |
    | examples | example   | example-1    | example is taken => example-1     |
    | examples | example   | example-2    | example is taken => example-2     |
    | examples | example-1 | example-1-1  | example-1 is taken => example-1-1 |
    | examples | example   | example-3    | example is taken => example-3     |
    | examples | example-2 | example-2-1  | example-2 is taken => example-2-1 |
    | examples | example-3 | example-3-1  | example-3 is taken => example-3-1 |
    | examples | example-4 | example-4    |                                   |
    | examples | example-4 | example-4-1  | example-4 is taken => example-4-1 |
    | examples | example   | example-5    | example is taken => example-2-1   |

    } );

my $document_uris = {}; # <document> => {uri} => count

foreach my $test ( @tests )
    {
    subtest( sprintf( "'%s#%s' from line %d", $test->{document}, $test->{base}, $test->{line} ),
            sub {
                note( $test->{comments} ) if $test->{comments};

                plan skip_all => $test->{skip} if $test->{skip};

                my $uris = $document_uris->{$test->{document}}
                       //= Markdown::URI::Register->new();

                is( $uris->uri_for_name( $test->{base} ), $test->{deduplicated}, "expected uri" );
                }
            );
    }

done_testing();

exit;
