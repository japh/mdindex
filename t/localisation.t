#!/usr/bin/env perl

# Test that the user can specify the word to use to identify indexes

# Known Problems:
#   - goldmark doesn't treat utf-8 characters as 'alphanumeric' :-(
#       - no umlaute etc.

use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";

use Test::More;
use Test::Differences;

use Markdown::Document;
use Markdown::URI::Goldmark;

my @tests = (

            {
            line    =>  __LINE__,
            name    =>  'default: index',
            given   =>  q{
                         |# Title
                         |# Index
                         |# TOC
                         |# Section 1
                         |# Section 2
                         |# Section 3
                         },
            then    =>  {
                        indexes  => [
                                    {
                                    uri   => 'index',
                                    level => 1
                                    }
                                    ],
                        markdown => q{
                                     |# Title
                                     |# Index
                                     |
                                     |- [TOC](#toc)
                                     |- [Section 1](#section-1)
                                     |- [Section 2](#section-2)
                                     |- [Section 3](#section-3)
                                     |
                                     |# TOC
                                     |# Section 1
                                     |# Section 2
                                     |# Section 3
                                     },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'table of contents: TOC',
            given   =>  q{
                         |# Title
                         |# Index
                         |# TOC
                         |# Section 1
                         |# Stock - no tocs here
                         |# Section 2
                         |# Section 3
                         },
            when    =>  {
                        index_re => qr/\btoc\b/i,
                        },
            then    =>  {
                        indexes  => [
                                    {
                                    uri   => 'toc',
                                    level => 1
                                    }
                                    ],
                        markdown => q{
                                     |# Title
                                     |# Index
                                     |# TOC
                                     |
                                     |- [Section 1](#section-1)
                                     |- [Stock - no tocs here](#stock---no-tocs-here)
                                     |- [Section 2](#section-2)
                                     |- [Section 3](#section-3)
                                     |
                                     |# Section 1
                                     |# Stock - no tocs here
                                     |# Section 2
                                     |# Section 3
                                     },
                        },
            },

            );

my $wip_only = grep { $_->{wip} } @tests;
foreach my $test ( @tests )
    {
    next if $wip_only and not $test->{wip};

    subtest( sprintf( "'%s' from line %d", $test->{name} // 'test', $test->{line} ),
            sub {
                note( $test->{comments} ) if $test->{comments};

                plan skip_all => $test->{skip} if $test->{skip};

                my $given = $test->{given} // '';
                my $when  = $test->{when}  // {};
                my $then  = $test->{then}  // {};

                my $before = (                      $given ) =~ s/^\h*\|?//gmr =~ s/^\v//r;
                my $after  = ( $then->{markdown} // $given ) =~ s/^\h*\|?//gmr =~ s/^\v//r;

                my $doc = Markdown::Document->new(
                                                 index_re   => $when->{index_re},
                                                 uri_mapper => Markdown::URI::Goldmark->new(),
                                                 );

                $doc->read( $before );

                eq_or_diff(
                          [ map {
                                    {
                                    uri   => $_->uri(),
                                    level => $_->level(),
                                    }
                                }
                            $doc->indexes()
                          ],
                          $then->{indexes} // [],
                          'expected document indexes',
                          );

                eq_or_diff(
                          $doc->markdown(),
                          $after,
                          'expected document markdown',
                          );

                },
            );
    }

fail( sprintf( "skipped %d non-wip tests", @tests - $wip_only ) ) if $wip_only;

done_testing();
exit;
