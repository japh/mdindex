#!/usr/bin/env perl

use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";

use Test::More;
use Test::Differences;

use Markdown::Document;
use Markdown::URI::Goldmark;

my @tests = (

            {
            line    =>  __LINE__,
            name    =>  'index with 2 relative, but non-sequential levels',
            given   =>  q{
                         |# Index -2
                         |### Level 3
                         |##### Level 5
                         |###### Level 6
                         |## Level 2
                         },
            then    =>  {
                        heading_uris => [qw(
                                        index
                                        level-3
                                        level-5
                                        level-6
                                        level-2
                                        )],
                        indexes      => [
                                        {
                                        uri   => 'index',
                                        level => 3,
                                        },
                                        ],
                        markdown     => q{
                                        |### Index
                                        |
                                        |- [Level 3](#level-3)
                                        |  - [Level 5](#level-5)
                                        |
                                        |### Level 3
                                        |##### Level 5
                                        |###### Level 6
                                        |## Level 2
                                        },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'embedded index',
            given   =>  q{
                         |# Index -3
                         |# Level 1
                         |### Level 3
                         |##### Level 5
                         |###### Level 6
                         |## Index -2
                         |### Level 3
                         |#### Level 4
                         },
            then    =>  {
                        heading_uris => [qw(
                                        index
                                        level-1
                                        level-3
                                        level-5
                                        level-6
                                        index-1
                                        level-3-1
                                        level-4
                                        )],
                        indexes      => [
                                        {
                                        uri   => 'index',
                                        level => 1,
                                        },
                                        {
                                        uri   => 'index-1',
                                        level => 3,
                                        },
                                        ],
                        markdown     => q{
                                        |# Index
                                        |
                                        |- [Level 1](#level-1)
                                        |  - [Level 3](#level-3)
                                        |  - [Index](#index-1)
                                        |  - [Level 3](#level-3-1)
                                        |    - [Level 4](#level-4)
                                        |
                                        |# Level 1
                                        |### Level 3
                                        |##### Level 5
                                        |###### Level 6
                                        |### Index
                                        |
                                        |- [Level 3](#level-3-1)
                                        |  - [Level 4](#level-4)
                                        |
                                        |### Level 3
                                        |#### Level 4
                                        },
                        },
            },

            );

my $wip_only = grep { $_->{wip} } @tests;
foreach my $test ( @tests )
    {
    next if $wip_only and not $test->{wip};

    subtest( sprintf( "'%s' from line %d", $test->{name} // 'test', $test->{line} ),
            sub {
                note( $test->{comments} ) if $test->{comments};

                plan skip_all => $test->{skip} if $test->{skip};

                my $given = $test->{given} // '';
                my $when  = $test->{when}  // {};
                my $then  = $test->{then}  // {};

                my $before = (                      $given ) =~ s/^\h*\|?//gmr =~ s/^\v//r;
                my $after  = ( $then->{markdown} // $given ) =~ s/^\h*\|?//gmr =~ s/^\v//r;

                my $doc = Markdown::Document->new( uri_mapper => Markdown::URI::Goldmark->new() );

                $doc->read( $before );

                eq_or_diff(
                          [ map { $_->uri() } $doc->headings() ],
                          $then->{heading_uris} // [],
                          'expected document headings',
                          );

                eq_or_diff(
                          [ map {
                                    {
                                    uri   => $_->uri(),
                                    level => $_->level(),
                                    }
                                }
                            $doc->indexes()
                          ],
                          $then->{indexes} // [],
                          'expected document indexes',
                          );

                eq_or_diff(
                          $doc->markdown(),
                          $after,
                          'expected document markdown',
                          );

                },
            );
    }

fail( sprintf( "skipped %d non-wip tests", @tests - $wip_only ) ) if $wip_only;

done_testing();
exit;
