#!/usr/bin/env perl

use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";

use Test::More;
use Test::Differences;

use Markdown::Document;
use Markdown::URI::Goldmark;

my @tests = (

            {
            line    =>  __LINE__,
            name    =>  'empty index',
            given   =>  q{
                        |# Title
                        |# Index
                        |# End
                        },
            then    =>  {
                        heading_uris => [qw(
                                        title
                                        end
                                        )],
                        indexes      => [],
                        markdown     => q{
                                        |# Title
                                        |# End
                                        },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'non-index',
            given   =>  q{
                        |# Index -2
                        |not a list item
                        |## Level 2
                        |### Level 3
                        |###### Level 6
                        |# End
                        },
            then    =>  {
                        heading_uris => [qw(
                                        index--2
                                        level-2
                                        level-3
                                        level-6
                                        end
                                        )],
                        indexes      => [],
                        markdown     => q{
                                        |# Index -2
                                        |not a list item
                                        |## Level 2
                                        |### Level 3
                                        |###### Level 6
                                        |# End
                                        },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'embedded non-index',
            given   =>  q{
                        |# Index -2
                        |## Level 2
                        |### Level 3
                        |# Not An Index
                        |This is not an index
                        |### Level 3
                        |#### Level 4
                        |###### Level 6
                        |#### Level 4
                        |###### Level 6
                        |# End
                        },
            then    =>  {
                        heading_uris => [qw(
                                        index
                                        level-2
                                        level-3
                                        not-an-index
                                        level-3-1
                                        level-4
                                        level-6
                                        level-4-1
                                        level-6-1
                                        end
                                        )],
                        indexes      => [
                                        {
                                        line       => 1,
                                        name       => 'Index',
                                        uri        => 'index',
                                        level      => 2,
                                        depth      => 2,
                                        size       => 2,
                                        is_index   => 1,
                                        is_heading => 1,
                                        },
                                        ],
                        markdown     => q{
                                        |## Index
                                        |
                                        |- [Level 2](#level-2)
                                        |  - [Level 3](#level-3)
                                        |
                                        |## Level 2
                                        |### Level 3
                                        |# Not An Index
                                        |This is not an index
                                        |### Level 3
                                        |#### Level 4
                                        |###### Level 6
                                        |#### Level 4
                                        |###### Level 6
                                        |# End
                                        },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'embedded empty index (same as previous, but with a list in second index)',
            given   =>  q{
                        |# Index -2
                        |## Level 2
                        |### Level 3
                        |# Empty Index
                        |-one level of detail
                        |### Level 3
                        |#### Level 4
                        |###### Level 6
                        |#### Level 4
                        |###### Level 6
                        |# End
                        },
            then    =>  {
                        heading_uris => [qw(
                                        index
                                        level-2
                                        level-3
                                        level-3-1
                                        level-4
                                        level-6
                                        level-4-1
                                        level-6-1
                                        end
                                        )],
                        indexes      => [
                                        {
                                        line       => 1,
                                        name       => 'Index',
                                        uri        => 'index',
                                        level      => 2,
                                        depth      => 2,
                                        size       => 3,
                                        is_index   => 1,
                                        is_heading => 1,
                                        },
                                        ],
                        markdown     => q{
                                        |## Index
                                        |
                                        |- [Level 2](#level-2)
                                        |  - [Level 3](#level-3)
                                        |  - [Level 3](#level-3-1)
                                        |
                                        |## Level 2
                                        |### Level 3
                                        |### Level 3
                                        |#### Level 4
                                        |###### Level 6
                                        |#### Level 4
                                        |###### Level 6
                                        |# End
                                        },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'embedded index (same as previous, but with 2-level second index)',
            given   =>  q{
                        |# Index
                        |## Level 2
                        |### Level 3
                        |# Embedded Index
                        |-first level
                        |  - second level
                        |### Level 3
                        |#### Level 4
                        |###### Level 6
                        |#### Level 4
                        |###### Level 6
                        |# End
                        },
            then    =>  {
                        heading_uris => [qw(
                                        level-2
                                        level-3
                                        embedded-index
                                        level-3-1
                                        level-4
                                        level-6
                                        level-4-1
                                        level-6-1
                                        end
                                        )],
                        indexes      => [
                                        {
                                        line       => 4,
                                        name       => 'Embedded Index',
                                        uri        => 'embedded-index',
                                        level      => 3,
                                        depth      => 2,
                                        size       => 3,
                                        is_index   => 1,
                                        is_heading => 1,
                                        },
                                        ],
                        markdown     => q{
                                        |## Level 2
                                        |### Level 3
                                        |### Embedded Index
                                        |
                                        |- [Level 3](#level-3-1)
                                        |  - [Level 4](#level-4)
                                        |  - [Level 4](#level-4-1)
                                        |
                                        |### Level 3
                                        |#### Level 4
                                        |###### Level 6
                                        |#### Level 4
                                        |###### Level 6
                                        |# End
                                        },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'code within a non-index',
            given   =>  q{
                        |# Title
                        |# Index
                        |```
                        |## Level 2
                        |### Level 3
                        |```
                        |# End
                        },
            then    =>  {
                        heading_uris => [qw(
                                        title
                                        index
                                        end
                                        )],
                        indexes      => [],
                        markdown     => q{
                                        |# Title
                                        |# Index
                                        |```
                                        |## Level 2
                                        |### Level 3
                                        |```
                                        |# End
                                        },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'recursively squashed indexes - each index appears to have 2 entries, until "Lower Index" gets squashed',
            given   =>  q{
                        |# Outer Index
                        |## Level 2
                        |## Inner Index
                        |### Level 3
                        |### Lower Index
                        |#### Level 4
                        |# End
                        },
            then    =>  {
                        heading_uris => [qw(
                                        level-2
                                        level-3
                                        level-4
                                        end
                                        )],
                        indexes      => [],
                        markdown     => q{
                                        |## Level 2
                                        |### Level 3
                                        |#### Level 4
                                        |# End
                                        },
                        },
            },

            );

my $wip_only = grep { $_->{wip} } @tests;
foreach my $test ( @tests )
    {
    next if $wip_only and not $test->{wip};

    subtest( sprintf( "'%s' from line %d", $test->{name} // 'test', $test->{line} ),
            sub {
                note( $test->{comments} ) if $test->{comments};

                plan skip_all => $test->{skip} if $test->{skip};

                my $given = $test->{given} // '';
                my $when  = $test->{when}  // {};
                my $then  = $test->{then}  // {};

                my $before = (                      $given ) =~ s/^\h*\|?//gmr =~ s/^\v//r;
                my $after  = ( $then->{markdown} // $given ) =~ s/^\h*\|?//gmr =~ s/^\v//r;

                my $doc = Markdown::Document->new( uri_mapper => Markdown::URI::Goldmark->new() );

                $doc->read( $before );

                eq_or_diff(
                          [ grep { $_ } map { $_->uri() } $doc->headings() ],
                          $then->{heading_uris} // [],
                          'expected document headings',
                          );

                eq_or_diff(
                          [ map {
                                    {
                                    line      => $_->line(),
                                    name      => $_->name(),
                                    uri       => $_->uri(),
                                    level     => $_->level(),
                                    depth     => $_->depth(),
                                    size      => scalar( $_->headings() ),
                                    is_index  => $_->is_index(),
                                    is_heading => $_->is_heading(),
                                    }
                                }
                            $doc->indexes()
                          ],
                          $then->{indexes} // [],
                          'expected document indexes',
                          );

                eq_or_diff(
                          $doc->markdown(),
                          $after,
                          'expected document markdown',
                          );

                },
            );
    }

fail( sprintf( "skipped %d non-wip tests", @tests - $wip_only ) ) if $wip_only;

done_testing();
exit;
