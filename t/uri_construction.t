#!/usr/bin/env perl

#
# test the construction of URIs for markdown headings
#
#   Rules:
#       - ignore surrounding whitespace
#       - empty heading name => 'Heading'
#       - heading name should retain all special characters
#       - heading uri is always lowercase
#       - uri may only contain alphanumeric and '-' characters
#           - all other characters are replaced with '-'
#           - '-'s are *not* squashed
#
#   Assumption:
#       the markdown scanner converts leading '#' or subsequent '===' or '---'
#       lines into a heading level number
#

use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";

use Test::More;
use PSV;

use Markdown::Heading;
use Markdown::URI::Goldmark;
use Markdown::URI::Github;

my @tests = PSV->table_from_string(
    {
    line_offset    => 4,
    list_of_hashes => 1,
    },
    q{

    | skip            | heading                  | level | name                   | goldmark            | github              | comments                    |
    | --------------- | ------------------------ | ----- | ---------------------- | ------------------- | ------------------- | --------------------------- |

    Empty headings get the name 'Heading' by default                                                    

    |                 | #                        | 1     | Heading                | heading             | heading             | empty document title        |
    |                 | ##                       | 2     | Heading                | heading             | heading             | empty heading               |
    |                 | ###                      | 3     | Heading                | heading             | heading             |                             |
    |                 | ####                     | 4     | Heading                | heading             | heading             |                             |
    |                 | #####                    | 5     | Heading                | heading             | heading             |                             |
    |                 | ######                   | 6     | Heading                | heading             | heading             |                             |

    All URI's are lowercase                                                                             

    |                 | # Hi                     | 1     | Hi                     | hi                  | hi                  |                             |
    |                 | ## Hi                    | 2     | Hi                     | hi                  | hi                  |                             |
    |                 | ### Hi                   | 3     | Hi                     | hi                  | hi                  |                             |
    |                 | #### Hi                  | 4     | Hi                     | hi                  | hi                  |                             |
    |                 | ##### Hi                 | 5     | Hi                     | hi                  | hi                  |                             |
    |                 | ###### Hi                | 6     | Hi                     | hi                  | hi                  |                             |

    Leading and trailing whitespace is ignored                                                          

    |                 | "#       Hi     "        | 1     | Hi                     | hi                  | hi                  |                             |
    |                 | "##      Hi     "        | 2     | Hi                     | hi                  | hi                  |                             |
    |                 | "###     Hi     "        | 3     | Hi                     | hi                  | hi                  |                             |
    |                 | "####    Hi     "        | 4     | Hi                     | hi                  | hi                  |                             |
    |                 | "#####   Hi     "        | 5     | Hi                     | hi                  | hi                  |                             |
    |                 | "######  Hi     "        | 6     | Hi                     | hi                  | hi                  |                             |

    Embedded non-word characters are translated to '-'s                                                 

    |                 | # "Hi!   There"          | 1     | ""Hi!   There""        | -hi----there-       | hi-there            |                             |
    |                 | ## "Hi!   There"         | 2     | ""Hi!   There""        | -hi----there-       | hi-there            |                             |
    |                 | ### "Hi!   There"        | 3     | ""Hi!   There""        | -hi----there-       | hi-there            |                             |
    |                 | #### "Hi!   There"       | 4     | ""Hi!   There""        | -hi----there-       | hi-there            |                             |
    |                 | ##### "Hi!   There"      | 5     | ""Hi!   There""        | -hi----there-       | hi-there            |                             |
    |                 | ###### "Hi!   There"     | 6     | ""Hi!   There""        | -hi----there-       | hi-there            |                             |

    |                 | # 1st. Level             | 1     | 1st. Level             | 1st--level          | 1st-level           | '.' is not a word-character |
    |                 | # Top Heading:           | 1     | Top Heading:           | top-heading-        | top-heading         |                             |

    |                 | # Section 1.2.3          | 1     | Section 1.2.3          | section-123         | section-1-2-3       |                             |

    Strip fomatting characters                     1                                                    

    | not implemented | # *Italic Heading*       | 1     | *Italic Heading*       | italic-heading      | italic-heading      |                             |
    | not implemented | # **Bold Heading**       | 1     | **Bold Heading**       | bold-heading        | bold-heading        |                             |
    | not implemented | # `-option` Description: | 1     | `-option` Description: | -option-description | -option-description |                             |

    } );

foreach my $test ( @tests )
    {
    subtest( sprintf( "'%s' from line %d", $test->{heading}, $test->{line} ),
            sub {
                note( $test->{comments} ) if $test->{comments};

                plan skip_all => $test->{skip} if $test->{skip};

                my $goldmark = Markdown::URI::Goldmark->new();
                my $github   = Markdown::URI::Github->new();

                $test->{heading} =~ /^(?<level>#+)\s*(?<name>.*?)\s*$/;
                my $level = length( $+{level} // "" );
                my $name  = $+{name};
                my $hdg   = Markdown::Heading->new(
                                                  level => $level,
                                                  name  => $name ne "" ? $name : undef,
                                                  );

                is( $hdg->level(), $test->{level}, "expected level" );
                is( $hdg->name(),  $test->{name},  "expected name"  );
                is( $goldmark->base_uri_from_name( $hdg->name() ), $test->{goldmark}, "expected goldmark uri" );
                is( $github->base_uri_from_name( $hdg->name()   ), $test->{github},   "expected github uri"   );
                }
            );
    }

done_testing();

exit;
