#!/usr/bin/env perl

#
# Test the interpretation and re-building of markdown documents
#
# Rules:
#
#   - no lines should be removed or changed
#       - except in existing 'Index' sections
#
#   - new indexes should not be created
#       - unless the 'create_index' option is set
#
#   - existing indexes should be re-built
#

use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";

use Test::More;
use Test::Differences;

use Markdown::Document;
use Markdown::URI::Goldmark;

my @tests = (

            {
            line    =>  __LINE__,
            name    =>  'empty document',
            given   =>  '',
            then    =>  {
                        heading_uris => [],
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'single title',
            given   =>  q{
                         |# Title
                         |
                         |some text
                         |blah blah
                         },
            then    =>  {
                        heading_uris => [ 'title' ],
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'various heading types',
            given   =>  q{
                         |# Title
                         |## Level 2
                         |### Level 3
                         |#### Level 4
                         |##### Level 5
                         |###### Level 6
                         |
                         |Level 1
                         |=
                         |Level 2
                         |----
                         |-------
                         },
            then    =>  {
                        heading_uris => [qw(
                                        title
                                        level-2
                                        level-3
                                        level-4
                                        level-5
                                        level-6
                                        level-1
                                        level-2-1
                                        )],
                        markdown    => q{
                                        |# Title
                                        |## Level 2
                                        |### Level 3
                                        |#### Level 4
                                        |##### Level 5
                                        |###### Level 6
                                        |
                                        |Level 1
                                        |=======
                                        |Level 2
                                        |-------
                                        |-------
                                        },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'multiple headings',
            given   =>  q{
                         |# Title
                         |
                         |some text
                         |blah blah
                         |
                         |# Section 1
                         |
                         |section 1 text
                         |
                         |# Section 2
                         |
                         |section 2 text
                         },
            then    =>  {
                        heading_uris => [qw(
                                        title
                                        section-1
                                        section-2
                                        )],
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'an existing, but empty index',
            given   =>  q{
                         |# Title
                         |
                         |some text
                         |blah blah
                         |
                         |# Index
                         |
                         |# Section 1
                         |
                         |section 1 text
                         |
                         |# Section 2
                         |
                         |section 2 text
                         },
            then    =>  {
                        heading_uris => [qw(
                                        title
                                        index
                                        section-1
                                        section-2
                                        )],
                        indexes      => [
                                        {
                                        uri   => 'index',
                                        level => 1,
                                        },
                                        ],
                        markdown     =>  q{
                                          |# Title
                                          |
                                          |some text
                                          |blah blah
                                          |
                                          |# Index
                                          |
                                          |- [Section 1](#section-1)
                                          |- [Section 2](#section-2)
                                          |
                                          |# Section 1
                                          |
                                          |section 1 text
                                          |
                                          |# Section 2
                                          |
                                          |section 2 text
                                          },
                        },
            },

            {
            line    =>  __LINE__,
            name    =>  'implicitly update a level 3 index with existing structure',
            given   =>  q{
                         |# Title
                         |## Level 2
                         |### Level 3
                         |#### Level 4
                         |##### Level 5
                         |###### Level 6
                         |## Level 2
                         |### Index
                         |- level 3 placeholder
                         |  - level 4 placeholder
                         |    - level 5 placeholder
                         |### Level 3
                         |#### Level 4
                         |##### Level 5
                         |###### Level 6
                         },
            when    =>  {
                        },
            then    =>  {
                        heading_uris => [qw(
                                        title
                                        level-2
                                        level-3
                                        level-4
                                        level-5
                                        level-6
                                        level-2-1
                                        index
                                        level-3-1
                                        level-4-1
                                        level-5-1
                                        level-6-1
                                        )],
                        indexes      => [
                                        {
                                        uri   => 'index',
                                        level => 3,
                                        },
                                        ],
                        markdown     =>  q{
                                          |# Title
                                          |## Level 2
                                          |### Level 3
                                          |#### Level 4
                                          |##### Level 5
                                          |###### Level 6
                                          |## Level 2
                                          |### Index
                                          |
                                          |- [Level 3](#level-3-1)
                                          |  - [Level 4](#level-4-1)
                                          |    - [Level 5](#level-5-1)
                                          |
                                          |### Level 3
                                          |#### Level 4
                                          |##### Level 5
                                          |###### Level 6
                                          },
                        },
            },

            );

my $wip_only = grep { $_->{wip} } @tests;
foreach my $test ( @tests )
    {
    next if $wip_only and not $test->{wip};

    subtest( sprintf( "'%s' from line %d", $test->{name} // 'test', $test->{line} ),
            sub {
                note( $test->{comments} ) if $test->{comments};

                plan skip_all => $test->{skip} if $test->{skip};

                my $given = $test->{given} // '';
                my $when  = $test->{when}  // {};
                my $then  = $test->{then}  // {};

                my $before = (                      $given ) =~ s/^\h*\|?//gmr =~ s/^\v//r;
                my $after  = ( $then->{markdown} // $given ) =~ s/^\h*\|?//gmr =~ s/^\v//r;

                my $doc = Markdown::Document->new( uri_mapper => Markdown::URI::Goldmark->new() );

                $doc->read( $before );

                eq_or_diff(
                          [ map { $_->uri() } $doc->headings() ],
                          $then->{heading_uris} // [],
                          'expected document headings',
                          );

                eq_or_diff(
                          [ map {
                                    {
                                    uri   => $_->uri(),
                                    level => $_->level(),
                                    }
                                }
                            $doc->indexes()
                          ],
                          $then->{indexes} // [],
                          'expected document indexes',
                          );

                eq_or_diff(
                          $doc->markdown(),
                          $after,
                          'expected document markdown',
                          );

                },
            );
    }

fail( sprintf( "skipped %d non-wip tests", @tests - $wip_only ) ) if $wip_only;

done_testing();
exit;
