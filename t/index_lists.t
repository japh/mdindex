#!/usr/bin/env perl

#
# Test the construction of a markdown index
#
# Steps:
#   create an index
#       specify name, level and detail levels
#   add headings
#   print index as markdown
#

use strict;
use warnings;

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";

use Test::More;

use Markdown::Index;
use Markdown::Heading;

my @tests = (

            {
            line  => __LINE__,
            name  => 'empty index (self-squashed due to having only 1 entry)',
            given => q{
                      |# Title
                      |## Level 2
                      |### Level 3
                      |#### Level 4
                      |##### Level 5
                      |###### Level 6
                      },
            then  => '',
            },

            {
            line  => __LINE__,
            name  => 'index with detail level in name',
            given => q{
                      |# Title
                      |## Level 2
                      |### Level 3
                      |#### Level 4
                      |##### Level 5
                      |###### Level 6
                      },
            when  =>  {
                      name => 'Index -2',
                      },
            then  => q{
                      |# Index
                      |
                      |- [Title](#)
                      |  - [Level 2](#)
                      |
                      },
            },

            {
            line  => __LINE__,
            name  => 'typical index on level 2',
            given => q{
                      |## Level 2
                      |## Level 2
                      |### Level 3
                      |### Level 3
                      |#### Level 4
                      |#### Level 4
                      |##### Level 5
                      |##### Level 5
                      |###### Level 6
                      |###### Level 6
                      },
            then  => q{
                      |## Index
                      |
                      |- [Level 2](#)
                      |- [Level 2](#)
                      |
                      },
            },

            {
            line  => __LINE__,
            name  => 'deeper index at level 2',
            given => q{
                      |## Level 2
                      |## Level 2
                      |### Level 3
                      |### Level 3
                      |#### Level 4
                      |#### Level 4
                      |##### Level 5
                      |##### Level 5
                      |###### Level 6
                      |###### Level 6
                      },
            when  => {
                     name => 'Index -2',
                     },
            then  => q{
                      |## Index
                      |
                      |- [Level 2](#)
                      |- [Level 2](#)
                      |  - [Level 3](#)
                      |  - [Level 3](#)
                      |
                      },
            },

            {
            line  => __LINE__,
            name  => 'index with 2 detail levels',
            given => q{
                      |## Level 2
                      |### Level 3
                      |#### Level 4
                      |##### Level 5
                      |###### Level 6
                      },
            when  => {
                     name => 'Index -2',
                     },
            then  => q{
                      |## Index
                      |
                      |- [Level 2](#)
                      |  - [Level 3](#)
                      |
                      },
            },

            {
            line  => __LINE__,
            name  => 'index with all levels of detail',
            given => q{
                      |# Title
                      |## Level 2
                      |### Level 3
                      |#### Level 4
                      |##### Level 5
                      |###### Level 6
                      },
            when  => {
                     name => 'Index -6',
                     },
            then  => q{
                      |# Index
                      |
                      |- [Title](#)
                      |  - [Level 2](#)
                      |    - [Level 3](#)
                      |      - [Level 4](#)
                      |        - [Level 5](#)
                      |          - [Level 6](#)
                      |
                      },
            },

            );

my $wip_only = grep { $_->{wip} } @tests;
foreach my $test ( @tests )
    {
    next if $wip_only and not $test->{wip};

    subtest( sprintf( "'%s' from line %d", $test->{name} // 'test', $test->{line} ),
            sub {
                note( $test->{comments} ) if $test->{comments};

                plan skip_all => $test->{skip} if $test->{skip};

                my $given = $test->{given} // '';
                my $when  = $test->{when}  // {};
                my $then  = $test->{then}  // '';

                my $idx  = Markdown::Index->new( heading => Markdown::Heading->new( name => $when->{name} // 'Index' ) );

                my $before = $given =~ s/^\h*\|?//gmr =~ s/^\v//r;
                my $after  = $then  =~ s/^\h*\|?//gmr =~ s/^\v//r;

                foreach my $line ( split( /\v/, $before ) )
                    {
                    $line =~ /^(?<level>#+)\s*(?<name>.*?)\s*$/;
                    my $heading = Markdown::Heading->new(
                                                        level => length( $+{level} ),
                                                        name  => $+{name},
                                                        );
                    $idx->add_heading( $heading );
                    }

                is( $idx->markdown(), $after, 'expected index' );
                },
            );
    }

fail( sprintf( "skipped %d non-wip tests", @tests - $wip_only ) ) if $wip_only;

done_testing();
exit;
