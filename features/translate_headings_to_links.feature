Ability: translate headings to links

  Headings are prefixed by a series of 1 to 6 '#' characters.
  The text of each heading is normalised by the rules:

  - convert all characters to lowercase
  - convert all non-word characters to '-'
  - squash all sequences of consecutive '-'s to a single '-'
  - remove leading and trailing '-'s
  - prefix the link with a '#' to indicate that the link refers to a
    fragment of the document, and not a separate document file

  Headings marked by a subsequnt row of '=' characters are equivelent to
  headings prefixed by a single '#'

  Headings marked by a subsequnt row of '-' characters are equivelent to
  headings prefixed by a two '##''s

  Scenario: derive links from heading text

    Given a heading with <text>
     When a link to the heading is created
     Then the link should be <link>
    Examples:
      | text                   | link                | comments                    |
      | hello                  | #hello              |                             |
      | hello world            | #hello-world        |                             |
      | Hello World            | #hello-world        |                             |
      | 1st. Level             | #1st-level          | '.' is not a word-character |
      | Top Heading:           | #top-heading        |                             |
      | *Italic Heading*       | #italic-heading     |                             |
      | `-option` Description: | #option-description |                             |

  Rule: duplicate headings must be numbered

    If the same heading appears multiple times, each repetition is given
    an increasing, numerical suffix.

    The check for duplication should only use the normalised link, not
    the original heading text.

    Scenario: add count suffix for duplicate headings

      Given a document
            """
            # Title
            ## Example:
            ## Example:
            ## Example
            ## Examples
            """
       When the document is indexed
       Then the indexed document should be
            """
            # Title

            ## Index

            - [Example:](#example)
            - [Example:](#example-1)
            - [Example](#example-2)
            - [Examples](#examples)
            - [Example 1:](#example-1-1)
            - [Example 1](#example-1-2)

            ## Examples:
            ## Examples:
            ## Examples
            ## Examples
            ## Example 1:
            ## Example 1
            """

