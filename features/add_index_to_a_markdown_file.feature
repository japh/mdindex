Ability: add an index to a markdown file

  When a markdown document has 'many sections'
  (for a subjective number of 'many'), an index near the beginning of
  the document can give readers an overview of the document's
  structure, and allow them to quickly navigate to their section of
  interest.

  Indexes are implemented by `mdindex` as a hierarchical list of lists
  with *fragment links* for each item in the index.

  The `level` of a heading is used to determine its indentation within
  the index.

  Heading levels begin at level 0 for headings prefixed by '#' or
  underlined with '=' characters (the document title).

  Headings with the prefix '##' or underlined with '-' characters are
  level 1 headings.

  Headings with the prefix '###' are level 2 headings, and so on...

  Indexes are described by their 'top level' and 'depth' (or 'level of
  detail').

  The 'top level' (or just 'level') of an index is the level of the
  first heading displayed, and is also used for the heading of the index
  itself.

  The 'level of detail' of an index is the total number of levels displayed
  in the index.

  Each `level of detail` is indented by 4 spaces.

  e.g. a 'level 2 index with 2 levels of detail' might look something
  like this:

  |### Index
  |
  |- [Level 2 Heading](#level-2-heading)
  |    - [Level 3 Heading](#level-3-heading)
  |- [Level 2 Heading](#level-2-heading-1)
  |    - [Level 3 Heading](#level-3-heading-1)

  Rule: by default, indexes are added just before the second heading of a document

    The first heading of a file is assumed to be the 'title',
    followed directly by an abstract or introduction (without a heading).
    All other headings are assumed to start new sections of the document at
    various levels of importance.

    Scenario: add a top-level index for an ideal document

      See also the "translate headings to links" feature

      Create an index of all '##' headings, just before the first '##'
      heading.

      Given a markdown document
            """
            # Title
            ## Level 1 Heading
            ### Level 2 Heading
            ## Level 1 Heading
            ### Level 2 Heading
            #### Level 3 Heading
            ##### Level 4 Heading
            ###### Level 5 Heading
            ## Level 1 Heading
            """
       When creating a level 1 index
       Then the indexed document should be
            """
            # Title

            ## Index

            - [Level 1 Heading](#level-1-heading)
            - [Level 1 Heading](#level-1-heading-1)
            - [Level 1 Heading](#level-1-heading-2)

            ## Level 1 Heading
            ### Level 2 Heading
            ## Level 1 Heading
            ### Level 2 Heading
            #### Level 3 Heading
            ##### Level 4 Heading
            ###### Level 5 Heading
            ## Level 1 Heading
            """

  Rule: the index level should be the same as the top-level being indexed

    Scenario: add a top-level index for a 1-level document

      If a document has multiple top-level headings, create an index
      just before the second heading.

      Note: this is the same as the previous scenario, but with one
      fewer '#'s per heading

      Given a markdown document
            """
            # Title
            # Level 1 Heading
            ## Level 2 Heading
            # Level 1 Heading
            ## Level 2 Heading
            ### Level 3 Heading
            #### Level 4 Heading
            ##### Level 5 Heading
            # Level 1 Heading
            """
       When creating a level 1 index
       Then the indexed document should be
            """
            # Title

            # Index

            - [Level 1 Heading](#level-1-heading)
            - [Level 1 Heading](#level-1-heading-1)

            # Level 1 Heading
            ## Level 2 Heading
            # Level 1 Heading
            ## Level 2 Heading
            ### Level 3 Heading
            #### Level 4 Heading
            ##### Level 5 Heading
            # Level 1 Heading
            """

  Rule: lower level headings must be indented in the index

    Scenario: create a multi-level index

      Given a markdown document
            """
            # Title
            ## Level 1 Heading
            ### Level 2 Heading
            ## Level 1 Heading
            ### Level 2 Heading
            #### Level 3 Heading
            ##### Level 4 Heading
            ###### Level 5 Heading
            ## Level 1 Heading
            """
       When creating a 3-tier, level 1 index
       Then the indexed document should be
            """
            # Title

            ## Index

            - [Level 1 Heading](#level-1-heading)
                - [Level 2 Heading](#level-2-heading)
            - [Level 1 Heading](#level-1-heading-1)
                - [Level 2 Heading](#level-2-heading-2)
                    - [Level 3 Heading](#level-3-heading)
            - [Level 1 Heading](#level-1-heading-2)

            ## Level 1 Heading
            ### Level 2 Heading
            ## Level 1 Heading
            ### Level 2 Heading
            #### Level 3 Heading
            ##### Level 4 Heading
            ###### Level 5 Heading
            ## Level 1 Heading
            """

