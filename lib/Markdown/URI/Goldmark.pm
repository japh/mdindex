package Markdown::URI::Goldmark;

use strict;
use warnings;

sub new { bless {}, shift }
sub base_uri_from_name
    {
    my $self = shift;
    my $uri  = shift;   # name to be turned into a uri

    # goldmark's heading id algorithm removes `...`, *...* and _..._, etc.
    # but I'm not exactly sure when
    $uri =~ s/\.(\d)/$1/g;  # 1.2.3 => 123
    $uri =~ s/\W/-/g;       # a b c => a-b-c
    $uri =  lc $uri;

    return $uri;
    }

1;
