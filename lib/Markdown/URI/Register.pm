package Markdown::URI::Register;

use strict;
use warnings;

sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self = bless {}, $class;

    $self->{mapper} = $param->{using};
    $self->{uris}   = {};

    return $self;
    }

sub uri_for_name
    {
    my $self = shift;
    my $name = shift;

    my $uris = $self->{uris};
    my $base = $self->{mapper}
                ? $self->{mapper}->base_uri_from_name( $name )
                : $name;
    my $uri  = $base;
    while( my $repeat = $uris->{$base}++ )
        {
        $uri = sprintf( "%s-%d", $base, $repeat );
        last if not $uris->{$uri};
        }
    $uris->{$uri} = 1;

    return $uri;
    }

1;
