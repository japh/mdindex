package Markdown::URI::Github;

use strict;
use warnings;

sub new { bless {}, shift }
sub base_uri_from_name
    {
    my $self = shift;
    my $uri  = shift;   # name to be turned into a uri

    $uri =~ s/\W/-/g;
    $uri =~ s/^-+|-+$//g;
    $uri =~ s/-+/-/g;
    $uri =  lc $uri;

    return $uri;
    }

1;
