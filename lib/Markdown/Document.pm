package Markdown::Document;

#
# Steps in processing a markdown document:
#
# Steps |--->
#
#       | 1. document | 2. code | 3. headings | 3b. index level | 4. indexes | 5. content | 6. index headings | 7. levels | 8. markdown |
#       | # title     |         | # title     |                 |            |            |                   |           | # title     |
#       | text        |         |             |                 |            |            |                   |           | text        |
#       | ```         | skip    |             |                 |            |            |                   |           | ```         |
#       | # index     | skip    |             |                 |            |            |                   |           | # index     |
#       | ## hello    | skip    |             |                 |            |            |                   |           | ## hello    |
#       | ### bye     | skip    |             |                 |            |            |                   |           | ### bye     |
#       | ```         | skip    |             |                 |            |            |                   |           | ```         |
#       | # index -2  |         | #index -2   |                 | # index    | depth=2    |                   | ## index  | ## index    |
#       |             |         |             |                 |            |            |                   | > hello   | > hello     |
#       |             |         |             |                 |            |            |                   | >> bye    | >> bye      |
#       |             |         |             |                 |            |            |                   | > hello   | > hello     |
#       |             |         |             |                 |            |            |                   | >> bye    | >> bye      |
#       |             |         |             |                 |            |            |                   | >> index  | >> index    |
#       |             |         |             |                 |            |            |                   | >> foo    | >> foo      |
#       | ## hello    |         | ## hello    | level=2         |            |            | ## hello          |           | ## hello    |
#       | ### bye     |         | ### bye     |                 |            |            | ### bye           |           | ### bye     |
#       | ## hello    |         | ## hello    |                 |            |            | ## hello          |           | ## hello    |
#       | ### bye     |         | ### bye     |                 |            |            | ### bye           |           | ### bye     |
#       | # index     |         | # index     |                 | # index    |            | # index           | ### index | ### index   |
#       | >-          |         |             |                 |            | depth=1    |                   | > foo     | > foo       |
#       | >>-         |         |             |                 |            | depth=2    |                   | >> bar    | >> bar      |
#       | ### foo     |         | ### foo     | level=3         |            |            | ### foo           |           | ### foo     |
#       | ###### bar  |         | ###### bar  |                 |            |            | ###### bar        |           | #### bar    |
#       | # end       |         | # end       |                 |            |            | # end             |           | # end       |
#
#   1.  scan each line of the original document
#   2.  skip code blocks
#   3.  identify headings
#       3a. underlined headings need to backtrack 1 step
#       3b. if previous heading was an index - set index level to level of this heading
#   4.  identify indexes via regex on heading name
#       4a. index level is initially 0
#       4b. set explicit index depth from name if present
#   5.  scan content of index
#       5a. reduce index to normal heading if non list content is found
#       5b. update indexes implied level of detail from list indentation levels
#   6.  add headings to index
#       6a. stop if index is above indexes' level (out of scope)
#   7.  determine how many levels of indentation are required,
#       7a. indexed headings only need to be 'relatively' below the index
#       7b. create 1 level of indent per heading level in each index
#   8.  render the markdown
#       8a. print normal lines unchanged
#       8b. print headings using their level (e.g. indexes get the right #-prefix)
#       8c. if index has non list content - print following text
#       8d. otherwise, generate index
#           8d1. print new list of links to indexed headings
#           8d2. skip original content of index
#

#
# Recursive Index Resolution:
#
#   indexes may appear within indexes!
#   e.g.
#
#       # Top Index
#       ## Level 2
#       ## Inner Index
#       ### Level 3
#       #### Level 4
#
#   In this case, the 'Top Index' should only contain 'Level 2' and a link to the 'Inner Index',
#   however, the 'Inner Index' only has 1 link, to 'Level 3', so the 'Inner
#   Index' would end up having just 1 link => and needs to be squashed, which
#   leaves 'Top Index' with just 1 link, thus it also needs to be squashed.
#   Thus, the result of the above would be:
#
#       ## Level 2
#       ### Level 3
#       #### Level 4
#
# For this reason, heading ID's *must* be assigned as late as possible, in
# order to retain the order that they will have when being processed by the
# final markdown engine.
#

use strict;
use warnings;

use Markdown::Index;
use Markdown::Heading;
use Markdown::URI::Register;

sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self = bless {}, $class;

    $self->{index_re}   = $param->{index_re} // qr/\bindex\b/i;
    $self->{uri_mapper} = $param->{uri_mapper};
    $self->{lines}      = [];

    return $self;
    }

sub read
    {
    my $self = shift;

    $self->scan( @_ );
    $self->add_headers_to_indexes();
    $self->finalise_indexes();
    $self->assign_uris();

    return;
    }

sub scan
    {
    my $self = shift;

    $self->{lines}   = [];

    my $index;
    my $in_code_block = 0;
    my $line_nr = 0;
    foreach my $text ( map { split( /^/ ) } @_ )
        {
        $line_nr++;

        # identify headings
        if( $text =~ /^\s*[`~]{3}/ )
            {
            $in_code_block = not $in_code_block;
            }

        my $heading;
        if( not $in_code_block )
            {
            # Match '# ...' style headings
            if( $text =~ /^(?<level>#+)\s*(?<name>.*?)\s*$/ )
                {
                $heading = Markdown::Heading->new(
                                                 line  => $line_nr,
                                                 name  => $+{name},
                                                 level => length( $+{level} ),
                                                 );
                }

            # Match underline-style headings
            # e.g.
            #   Heading or Heading
            #   =======    -------
            elsif( $text =~ /^(?<style>[=-])\g{style}*\s*$/ )
                {
                my $style = $+{style};

                # horizontal line only used as a heading if previous line was plain text
                if( $line_nr > 1                        # we have a previous line
                    and not( ref $self->{lines}[-1] )   # the previous line was 'nothing special'
                    and $self->{lines}[-1] =~ /\S/      # the previous line was not empty
                  ) {
                    my $heading_text = pop( @{$self->{lines}} );
                    $index->revoke_content( $line_nr-1 ) if $index;
                    $heading = Markdown::Heading->new(
                                                     line  => $line_nr,
                                                     name  => $heading_text,
                                                     style => $style,
                                                     );
                    }
                # else just treat this as a normal text, separator line
                }

            if( $heading )
                {
                # content of any section finishes at the next heading
                # (incl. previous index, if we had one)
                undef $index;

                if( $heading->name() =~ /$self->{index_re}/ )
                    {
                    $index = Markdown::Index->new( heading => $heading );
                    push @{$self->{lines}}, $index;
                    next;
                    }

                push @{$self->{lines}}, $heading;
                next;
                }
            }

        $index->add_content( $line_nr, $text ) if $index;
        push @{$self->{lines}}, $text;
        }

    return;
    }

# add headings to indexes
# implemented as an extra step to avoid special cases for nested indexes
sub add_headers_to_indexes
    {
    my $self = shift;

    my $active_index = {};
    foreach my $heading ( $self->headings() )
        {
        foreach my $index ( values %{$active_index} )
            {
            if( not $index->add_heading( $heading ) )
                {
                delete $active_index->{$index};
                }
            }
        if( $heading->is_index() )
            {
            $active_index->{$heading} = $heading;
            }
        }

    return;
    }

sub finalise_indexes
    {
    my $self = shift;

    my $index_count = $self->indexes();
    if( not $index_count )
        {
        # TODO: 2022-02-10 keep message as a verbose warning/info message
        print "no indexes found in document\n";
        return;
        }

    $_->finalise() foreach $self->indexes();

    if( not $self->indexes() )
        {
        # TODO: 2022-02-10 keep message as a verbose warning/info message
        print "no indexes left in document\n";
        }

    return;
    }

# assign a URI to each heading
# Note: this MUST be done AFTER updating indexes, because any empty indexes
# will be removed - thus (possibly) changing the set of unique heading id's
sub assign_uris
    {
    my $self = shift;

    my $uris_register = Markdown::URI::Register->new( using => $self->{uri_mapper} );
    foreach my $heading ( $self->headings() )
        {
        $heading->set_uri( $uris_register->uri_for_name( $heading->name() ) );
        }

    return;
    }

# returns all headings AND indexes
sub headings
    {
    my $self = shift;
    return grep { ref and $_->is_heading() } @{$self->{lines}};
    }

# only used for testing
sub indexes
    {
    my $self = shift;
    return grep { ref and $_->is_index() } @{$self->{lines}};
    }

sub markdown
    {
    my $self = shift;

    my @lines;
    my $skip = 0;
    foreach my $line ( @{$self->{lines}} )
        {
        # skip the old content of updated indexes
        next if $skip and $skip--;

        # output each line
        if( ref $line )
            {
            # convert objects to strings
            # indexes will be multi-line
            $skip = $line->skip_lines();
            $line = $line->markdown();
            }
        push @lines, $line;
        }

    return join( "", @lines );
    }

1;
