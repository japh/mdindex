package Markdown::Index;

# Markdown::Index reads a single markdown file and creates or updates embedded
# section index(es).

#
# Indexes are:
#
#   Sections, beginning with a heading, followed by a list of links within the document.
#
#   An index's heading must match the index regular expression, by default: /\bindex\b/i
#   An index's contents must ONLY contain list items with document-internal links
#
#   Any line which doesn't match both of the above conditions is simply 'a normal line'.
#
#   A Markdown::Index object is created for each line which matches the first
#   rule (looks like it might be an index).
#
#   A Markdown::Index object may be in one of several states:
#
#       - initial state (implicitly, only while reading original document)
#           - level is 0 (undefined)
#           - it may have a depth defined in the index name, via the pattern /\s-\d\s*$/
#           - has no content
#           - has no indexed headings
#
#       - content state (implicitly, only while reading original document)
#           - each line between an index heading and the following heading may be either...
#               - a list item, beginning with a -
#               - a non-list line
#           - any non-list lines invalidate the index, causing it to be left unchanged
#
#       - heading-only
#           - if the index was invalidated, it becomes a header only
#           - the index's markdown becomes just the original heading markdown
#           - all original lines are printed as if they were normal content
#
#       - empty index
#           - if the index has less than 2 indexed headings then it gets squashed
#           - the index's markdown becomes an empty string (no header, no content)
#
#       - index
#           - the index has 2 or more indexed headings
#           - the index's markdown contains:
#               - a heading built from the index's derived level and name
#               - an empty line
#               - a hierarchical list of document links
#               - an empty line
#

# | content | headings | is index | is empty | level               | heading      | headings      |
# | 0       | 0        | 1        | 1        | 0                   | level        | -             |
# | 1       | -        | 0        | -        | from index heading  | heading      | -             |
# | 0       | <= 1     | 1        | 1        | 0                   | ''           | -             |
# | 0       | 2+       | 1        | 0        | from linked heading | level + name | indented list |

# actions:
#
#   create index from heading
#   add content ...
#   revoke content
#   add heading ...

use strict;
use warnings;

use Markdown::Heading;

sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self = bless {}, $class;
    $self->{is_finalised}  = 0;
    $self->{is_heading}    = 1; # may change during finalisation
    $self->{is_index}      = 1; # may change during finalisation
    $self->{heading}       = $param->{heading};
    $self->{name}          = $self->{heading}->name();
    $self->{level}         = 0;
    $self->{uri}           = undef;
    $self->{content}       = undef;
    $self->{content_lines} = 0;
    $self->{indents}       = {};
    $self->{depth}         = 0;
    $self->{indent_of}     = undef; # { <level> => <indent> }
    $self->{headings}      = [];

    # allow the level of detail to be specified via a depth marker
    # a depth marker is just a digit between 1 and 6 preceded ` -`
    #
    # e.g.: # Index Of Options -2
    #
    # - the depth marker is removed from the indexes' name
    # - if a depth marker is not present,
    #   the level of detail is implied from the number of indent levels in the index
    if( $self->name() =~ /(?<name>.*?)\s+-(?<depth>[1-6])\s*$/ )
        {
        $self->{name}  = $+{name};
        $self->{depth} = $+{depth};
        }

    return $self;
    }

sub info
    {
    my $self = shift;

    my $info = sprintf "%2d. %s+%d %s(%s)",
           $self->line(),
           '#' x $self->level(),
           $self->depth(),
           $self->name(),
           $self->is_index()     ? sprintf( "index %d x -...", scalar( @{$self->{headings}} ) )
           : $self->is_heading() ? "heading<index>"
           :                       "empty index";

    return $info;
    }

sub is_heading { shift->{is_heading}      }
sub is_index   { shift->{is_index}        }
sub line       { shift->{heading}->line() }
sub depth      { shift->{depth}           }
sub headings   { @{shift->{headings}}     }
sub indexes    { grep { $_->is_index() } @{shift->{headings}} }

sub name
    {
    my $self = shift;
    return $self->{name}            if $self->is_index();
    return $self->{heading}->name() if $self->is_heading();
    return '';
    }

sub level
    {
    my $self = shift;
    return $self->{level}            if $self->is_index();
    return $self->{heading}->level() if $self->is_heading();
    return 0
    }

# add a line of content between an index heading and the following heading
sub add_content
    {
    my $self = shift;
    my $line = shift;
    my $text = shift;

    $self->{content_lines}++;

    # TODO: 2022-02-07 special handling for tabs (0x08, ^I)?
    #                  didn't we agree in the '80s that tabs are Broken As Designed (BAD)?
    if( $text =~ /^(?<indent>\s*)-/ )
        {
        $self->{indents}{ $+{indent} // '' } = 1;
        }
    elsif( $text =~ /\S/ )
        {
        $self->{content} = $line;
        }

    return;
    }

# required to handle underlined headings - i.e. we need to take a step back
sub revoke_content
    {
    my $self = shift;
    my $line = shift;

    if( $self->{content} eq $line )
        {
        undef $self->{content};
        $self->{content_lines}--;
        }

    return;
    }

# add a heading to this index
# returns 1 if the index remains active (i.e. lower level header than the level of the index)
# returns 0 if the header was higher than the index's level - index has become inavalid
sub add_heading
    {
    my $self    = shift;
    my $heading = shift;

    return if not $self->is_index();

    # printf "%d. '%s' setting level to %d\n", $self->{heading}{line}, $self->{name}, $heading->level() if not $self->{level};
    $self->{level} = $heading->level() if not $self->{level};

    # only check level of headings if the new heading is not an index (which
    # has level 0 initially)
    # - the levels to keep are filtered out later in headings()
    if( $heading->level() )
        {
        # this heading is out of scope for this index - stop adding headers
        return 0 if $heading->level() < $self->level();
        }

    push @{$self->{headings}}, $heading;

    # keep adding headings
    return 1;
    }

# finalise is called AFTER all headings have been added to the index
# but BEFORE the index's uri has been determined.
# This gives the index a chance to decide:
#   - if it is ONLY a header for a non-index section
#   - or is is an index with a hierarchy of links
#   - or is is an empty index (don't claim a URI)
sub finalise
    {
    my $self = shift;

    return if $self->{is_finalised};
    $self->{is_finalised} = 1;

    my $indent = '  ' x $self->{level};

    # printf "%s%d. '%s' finalising...\n", $indent, $self->{heading}{line}, $self->{name};

    if( $self->{content} )
        {
        $self->{is_index} = 0;
        # TODO: 2022-02-10 keep message as a verbose warning/info message
        printf "%sline %d: '%s' has non-index content - not re-indexed\n",
               $indent,
               $self->line(),
               $self->name();
        return;
        }

    # finalise any embedded indexes
    $_->finalise() foreach $self->indexes();

    # collate a list of unique heading levels captured by this index
    my $headings = [];
    $self->{indent_of} = {};
    foreach my $heading ( @{$self->{headings}} )
        {
        # skip squashed indexes
        next if not $heading->is_heading();
        
        # terminate at first (index) out of this index's scope
        my $level = $heading->level();
        last if $level < $self->{level};    

        # track 'unique' heading levels
        $self->{indent_of}{$level} = undef;
        push @{$headings}, $heading;
        }

    # determine the desired depth of this index
    if( not $self->{depth} )
        {
        $self->{depth} = scalar %{$self->{indents}} || 1;
        }

    # sort the unique indentation levels
    my $indent_level = 0;
    foreach my $level ( sort keys %{$self->{indent_of}} )
        {
        $self->{indent_of}{$level} = $indent_level++;
        last if $indent_level >= $self->depth();
        }

    # use YAML;
    # printf "%s %d. '%s' indent map:\n%s\n",
    #     $indent,
    #     $self->{heading}{line},
    #     $self->{name},
    #     Dump( $self->{indent_of} );

    $self->{headings} = $headings;
    $headings = [];

    # filter out headings which are 'below our level of detail'
    foreach my $heading ( @{$self->{headings}} )
        {
        # printf "%schecking heading: >%s '%s'\n", $indent, $heading->level(), $heading->name();
        next if not defined $self->{indent_of}{ $heading->level() };
        # printf "%skept '%s'\n", $indent, $heading->name();
        push @{$headings}, $heading;
        }

    # determine whether we have anything worth indexing
    if( @{$headings} > 1 )
        {
        # printf "%sline %d: '%s' has %d headings to index\n",
        #        $indent,
        #        $self->line(),
        #        $self->name(),
        #        scalar( @{$headings} );
        # foreach my $heading ( @{$headings} )
        #     {
        #     printf "%s>%s %s\n", $indent, $heading->level(), $heading->name();
        #     }
        $self->{headings} = $headings;
        }
    else
        {
        # TODO: 2022-02-10 keep message as a verbose warning/info message
        printf "%sline %d: '%s' has %d headings to index - squashed\n",
               $indent,
               $self->line(),
               $self->name(),
               scalar( @{$headings} );
        $self->{is_index} = 0;
        $self->{is_heading} = 0;
        }

    # printf "%s%d. '%s' finalised\n", $indent, $self->{heading}{line}, $self->{name};

    return;
    }

sub set_uri { shift->{uri} //= shift }
sub uri     { shift->{uri} // '' }

sub skip_lines
    {
    my $self = shift;
    return $self->{content_lines} if $self->is_index() or not $self->is_heading();
    return 0;
    }

sub markdown
    {
    my $self  = shift;
    $self->finalise();
    return $self->index_markdown()      if $self->is_index();
    return $self->{heading}->markdown() if $self->is_heading();
    return '';
    }

sub index_markdown
    {
    my $self  = shift;

    # print the index's markdown code
    my $indent_step = '  ';
    return join( "",
                   '#' x $self->level(), ' ', $self->name(), "\n",
                   "\n",
                   (
                   map {
                       sprintf( "%s- [%s](#%s)\n",
                                  $indent_step x $self->{indent_of}{ $_->level() },
                                  $_->name(),
                                  $_->uri(),
                                  );
                       }
                   $self->headings(),
                   ),
                   "\n"
               );
    }

##### debugging utilities

sub log_call
    {
    my $self = shift;
    return if $self->{is_logging};
    $self->{is_logging}++;
    # push @{$self->{log}},
    printf "%s\n",
         join( " ",
            sprintf( "%s->%s()", $self->info(), (caller(1))[3] =~ s/.*://r ),
            @_ <= 1
                ? shift // ""
                : sprintf( shift // "", @_ )
            );
    $self->{is_logging}--;
    return;
    }

1;
