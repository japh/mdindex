package Markdown::Heading;

use strict;
use warnings;

sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self = bless {}, $class;

    $self->{line}  = $param->{line}  // 0;
    $self->{name}  = $param->{name}  // 'Heading';
    $self->{level} = $param->{level} // 1;      # 1 .. 6
    $self->{style} = $param->{style} // '#';    # '#', '=' or '-'
    $self->{uri}   = undef;

    $self->{name} =~ s/\v$//;

    if( $self->{style} ne '#' )
        {
        $self->{level} = $self->{style} eq '=' ? 1 : 2;
        }

    return $self;
    }

sub info
    {
    my $self = shift;
    return sprintf "%2d. %s %s", $self->line(), '#' x $self->level(), $self->name();
    }

sub is_heading { 1 }
sub is_index   { 0 }
sub skip_lines { 0 }
sub level      { shift->{level} }
sub line       { shift->{line}  }
sub name       { shift->{name}  }
sub set_uri    { shift->{uri} //= shift }
sub uri        { shift->{uri} // '' }
sub finalise   {}

sub markdown
    {
    my $self = shift;

    if( $self->{style} eq '#' or $self->{level} > 2 )
        {
        return sprintf( "%s %s\n",
                        '#' x $self->{level},
                        $self->{name},
                        );
        }
    else
        {
        return sprintf( "%s\n%s\n",
                        $self->{name},
                        ['=','-']->[$self->{level}-1] x length( $self->{name} ),
                        );
        }
    }

1;
