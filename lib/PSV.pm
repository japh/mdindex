#
# PSV - Pipe Separated Values
#
# This module is used to simplify test definitions by allowing them to be
# defined as simple tables of pipe-separated values.
#
# Decoding Rules:
#
#   - only parse lines which begin with '|' (preceded by optional whitespace)
#   - ignore separator lines (| --- | --- | ...) 
#   - split on space-padded '|'s
#       - KNOWN BUG: it is not possible to encapsulate '|' characters in data
#   - trim " (double-quotes) if they are the first and last character of a cell's data
#   - map columns to hash keys if the list_of_hashes option is set
#       - the first row is assumed to contain column names
#

package PSV;

use strict;
use warnings;

sub table_from_string
    {
    my $class = shift;

    my $self = bless {}, $class;
    $self->{line}       = (caller)[2];
    $self->{data}       = [];
    $self->{decoration} = [];

    ( $self->{opts} ) = grep { ref( $_  ) eq 'HASH' } @_;
    $self->{line} += $self->{opts}{line_offset} // 0;

    my ( $string ) = grep { not  ref } @_;

    $self->split_string( $string );

    return @{$self->{table}};
    }

sub split_string
    {
    my $self   = shift;
    my $string = shift;

    foreach my $text ( split( /\v/, $string ) )
        {
        $self->{line}++;;

        # separator lines are decoration
        next if $text =~ /^\s*(\|[\s-]+)*\|?\s*$/;

        my @fields = split( /\s*\|\s*/, $text, -1 );
        if( @fields > 1 and $fields[0] eq "" )
            {
            shift @fields;
            pop @fields if $fields[$#fields] eq "";

            # strip surrounding quotes
            # BUG: 2022-02-06 these quotes don't protect embedded '|'s
            foreach( @fields )
                {
                s/^"(.*)"$/$1/;
                }

            # only rows beginning with \s*\| are data rows
            push @{$self->{table}},
                 {
                 line   => $self->{line},
                 fields => \@fields,
                 };
            }
        }

    if( $self->{opts}{list_of_hashes} )
        {
        my $hdr  = shift @{$self->{table}};
        my @cols = @{$hdr->{fields}};
        $self->{table} = [
                         map {
                             my $row = {};
                             $row->{line} = $_->{line};
                             @{$row}{@cols} = @{$_->{fields}};
                             $row;
                             }
                         @{$self->{table}}
                         ];
        }

    return;
    }

1;
