# Test Data for `mdindex`

This directory contains a few sample documents to demonstrate how `mdindex`
works.

## Samples

- Simple Indexes
    - [before](before/simple.md)
    - [after](after/simple.md)
- Multi-Level Indexes
    - [before](before/multi-levels.md)
    - [after](after/multi-levels.md)
- Embedded Indexed
    - [before](before/embedded.md)
    - [after](after/embedded.md)
- Non-Indexes
    - [before](before/non-indexes.md)
    - [after](after/non-indexes.md)
- Empty Indexes
    - [before](before/empty-indexes.md)
    - [after](after/empty-indexes.md)
