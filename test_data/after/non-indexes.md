# Non-Indexes

To prevent accidental removal of actual documentation, `mdindex` will
*do nothing* to sections which do not look like indexes.

For an index to be generated in a section, it must fullfil 2 conditions:

1. The section heading *must* contain the word `index` (case insensitive).

    - the `-i` option may be used to specify a regular expression to
      use instead of `index`
    - the index word must always match on word boundaries
      - e.g. the word `indexation` will not match

2. The section must be *empty* (not counting blank lines) or *only*
   contain lines beginning with `-` or `*`

    - i.e. every line must look like it is part of a simple list

Any sections which do not match the above criteria are left unchanged.

Here are some non-indexes:

Compare this document
[before](../before/non-indexes.md) and
[after](../after/non-indexes.md) processing with `mdindex`.
 
## An Empty Section

## An Index With Content

The previous section will not generate an index, as the word `index`
does not appear in its heading.

This section does have `index` in its heading, but this text causes
the section to be treated as a normal section of documentation.
