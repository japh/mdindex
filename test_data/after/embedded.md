# Embedded Indexes

This document demonstrates the use of **indexes embedded within indexes**.

It is the same as the [Multi-Level Indexes](multi-levels.md) sample,
but with an additional index at the top level.

## Index

- [Section 1](#section-1)
- [Section 2](#section-2)

## Section 1

Compare this document
[before](../before/multi-levels.md#section-1) and
[after](../after/multi-levels.md#section-1) processing with `mdindex`.

### Section 1 Index

- [Section 1.1](#section-11)
  - [Section 1.1.1](#section-111)
  - [Section 1.1.2](#section-112)
- [Section 1.2](#section-12)

### Section 1.1

[Section 1 Index](#section-1-index) uses a `-n` suffix to indicate
that 2 levels of headings should be indexed.

If there are fewer levels of headings than the requested depth, the
index will simply shrink to match the available structure.

The `-n` suffix will be removed from the heading.

#### Section 1.1.1

Second level heading, should appear in
[Section 1 Index](#section-1-index)

##### Section 1.1.1.1

This sub-section is too deep to be indexed by
[Section 1 Index](#section-1-index)

#### Section 1.1.2

This section should also appear in
[Section 1 Index](#section-1-index)

### Section 1.2

Lorem ipsum

## Section 2

[Section 2 Index](#section-2-index) uses a *fake* list of links to
indicate how deep the index should be.

In this case, [Section 2 Index](#section-2-index) also specifies that
is should be a 2 level index.

As with the `-n` suffix, if fewer levels of headings are available
than the template requests, the index will shrink to match the
available structure.

Compare this document
[before](../before/multi-levels.md#section-2) and
[after](../after/multi-levels.md#section-2) processing with `mdindex`.

### Section 2 Index

- [Section 2.1](#section-21)
  - [Section 2.1.1](#section-211)
  - [Section 2.1.2](#section-212)
- [Section 2.2](#section-22)

### Section 2.1

[Section 2 Index](#section-2-index) uses a `-n` suffix to indicate
that 2 levels of headings should be indexed.

If there are fewer levels of headings than the requested depth, the
index will simply shrink to match the available structure.

The `-n` suffix will be removed from the heading.

#### Section 2.1.1

Second level heading, should appear in
[Section 2 Index](#section-2-index)

##### Section 2.1.1.1

This sub-section is too deep to be indexed by
[Section 2 Index](#section-2-index)

#### Section 2.1.2

This section should also appear in
[Section 2 Index](#section-2-index)

### Section 2.2

Lorem ipsum

# The End

This is not in the index because the first heading after the index is
a `##` heading, this one only has one `#`, so it is *out of the
indexes scope*.
