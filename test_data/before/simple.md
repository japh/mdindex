# Simple Indexes

This document demonstrates how `mdindex` can generate **simple
indexes** within your markdown documents.

## Section 1

Below is a typical index.

To add an index to a document, simply add a heading with the word
`Index` just above the first heading you wish to appear in the index.

All headings below each *index heading* are added to the index, up to
the first heading which has a lower level than the index (e.g. `#` is
the lowest level). This allows indexes to be *scoped* to individual
sections of your document.

### Level Adjustment Of Index Headings

The heading level (number of `#`'s) of each generated index is
adjusted to match the first heading that appears after it.

For example, the original markdown:

```markdown
# Title
# Index
## Section 1
## Section 2
```

would become:

```markdown
# Title
## Index
    ...
## Section 1
## Section 2
```

Compare this document
[before](../before/simple.md#section-1-index) and
[after](../after/simple.md#section-1-index) processing with `mdindex`.

# Section 1 Index

### Section 1.1

By default, each index lists the following heading and any other
heading at the same level, until a heading with a lower level is
found.

### Section 1.2

Lorem ipsum

#### Section 1.2.1

This section is a detail, so it is not included in
[Section 1 Index](#section-1-index) by default.

See the [Multi-Level Indexes](multi-levels.md) to see how an index
can be generated with more levels of detail.

## Section 2

Section 2 is *below* (i.e. has *fewer* `#`'s) the first heading indexed by
[Section 1 Index](#section-1-index).

As a result, this section:

- is not included in [Section 1 Index](#section-1-index)
- [Section 1 Index](#section-1-index) is closed, preventing any
  further headings being added to it, regardless of their level.

Compare this document
[before](../before/simple.md#section-2-index) and
[after](../after/simple.md#section-2-index) processing with `mdindex`.

# Section 2 Index

### Section 2.1

Lorem ipsum

### Section 2.2

Lorem ipsum

#### Section 2.2.1

Lorem ipsum

# The End

This is not in the index because the first heading after the index is
a `##` heading, this one only has one `#`, so it is *out of the
indexes scope*.
