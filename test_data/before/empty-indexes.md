# Empty Index Squashing

Indexes are a great way to navigate around a document, however, an empty index,
or an index with only one link, is just a waste of space and distracting.

Below are several examples of indexes which `mdindex` will
***remove*** because they do not provide any benefitial structure to
the document.

## Section 1

### An Index With Only One Heading

The index below is followed by a single `###` heading, before a `##` heading.

Since the index would only list one link the index is removed entirely.

Compare this document
[before](../before/empty-indexes.md#section-1) and
[after](../after/empty-indexes.md#section-1) processing with `mdindex`.

# Section 1 Index

### Section 1.1

Lorem Ipsum

## Section 2

### Hierarchical Index Squashing

Although this examples *appears* as if it should cause multiple levels of *index
squashing*, in fact each index is independent of any preceding indexes, as their
headings are all on different levels.

Since each index only contains 1 heading at its respective level, they are all
squashed independently.

Compare this document
[before](../before/empty-indexes.md#section-2) and
[after](../after/empty-indexes.md#section-2) processing with `mdindex`.

# Index 1
### Section 2.1
# Index 2
#### Section 2.1.1
# Index 3
##### Section 2.1.1.1
# Index 4
###### Section 2.1.1.1

## Section 3

### Recursive Index Squashing

Some constellations of multiple indexes can lead to recursive index squashing.

When an index includes another index, the second index is evaluated first.

This can lead to indexes being squashed recusively, in this case, `Index 4` is
squashed because it would only list `Section 3.1`. Then `Index 3`, `Index 2`
and finally `Index 1` are also squashed, as all of their respective indexes
have been squashed.

Compare this document
[before](../before/empty-indexes.md#section-3) and
[after](../after/empty-indexes.md#section-3) processing with `mdindex`.

# Index 1
# Index 2
# Index 3
# Index 4
### Section 3.1
#### Section 3.1.1
##### Section 3.1.1.1
###### Section 3.1.1.1.1

## Section 4

### Duplicate Indexes

Duplicate index sections are probably not *wise*, but they will become
duplicate *generated* indexes.

If this happens, just delete one of the indexes and try again.

Compare this document
[before](../before/empty-indexes.md#section-4) and
[after](../after/empty-indexes.md#section-4) processing with `mdindex`.

# Index
# Index
## Section 4.1
## Section 4.2

# The End

There should always be an end to a good document...

... so that readers know when to stop reading.
